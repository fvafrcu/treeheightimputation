Dear CRAN Team,
this is a resubmission of package 'treeHeightImputation'. I have added the following changes:

* Added `impute_heights()` to impute heights to incomplete measured (field) data.

Please upload to CRAN.
Best, 

# Package treeHeightImputation 0.5.0

Reporting is done by packager version 1.15.2


## Test environments
- R Under development (unstable) (2023-09-23 r85215)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 5 (daedalus)
   0 errors | 2 warnings | 1 note 
- gitlab.com
  R version 4.3.1 (2023-06-16)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 22.04.3 LTS
  Package: treeHeightImputation
  Version: 0.5.0
  Status: 2 WARNINGs, 1 NOTE
- win-builder (devel)

## Local test results
- RUnit:
    treeHeightImputation_unit_test - 5 test functions, 0 errors, 0 failures in 13 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- Coverage by covr:
    treeHeightImputation Coverage: 52.78%

## Local meta results
- Cyclocomp:
     no issues.
- lintr:
    found 95 lints in 353 lines of code (a ratio of 0.2691).
- cleanr:
    found 2 dreadful things about your code.
- codetools::checkUsagePackage:
    found 4 issues.
- devtools::spell_check:
    found 8 unkown words.
