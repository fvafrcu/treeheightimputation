# treeHeightImputation 0.5.0

* Added `impute_heights()` to impute heights to incomplete measured (field) data.

# treeHeightImputation 0.4.0

* Having written package regulaFalsi, I choose to use stats::uniroot() only.
  It's faster, more reliable and thoroughly tested.

# treeHeightImputation 0.3.0

* Change argument _regula\_falsi_ to _method_, allowing for stats::uniroot().

# treeHeightImputation 0.2.2

* Set regula\_falsi = TRUE by default.

# treeHeightImputation 0.2.1

* Fixed hg(..., regula\_falsi = TRUE).

# treeHeightImputation 0.2.0

* Added regula falsi.

# treeHeightImputation 0.1.0

* Added a `NEWS.md` file to track changes to the package.



