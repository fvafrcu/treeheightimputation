get_uniform_species_codes <- function() {
    res <- utils::read.csv(system.file("extdata", "ehk.csv",
                                       package = "treeHeightImputation"))
    return(res)
}
get_species_codes <- function() {
    res <- utils::read.csv(system.file("extdata", "baumarten.csv",
                                       package = "treeHeightImputation"))
    res[["kuerzel"]] <-  fritools::convert_umlauts_to_ascii(res[["kuerzel"]])
    return(res)
}
get_species_codes_relation <- function() {
    res <- utils::read.csv(system.file("extdata", "baumartzuordnungen.csv",
                                       package = "treeHeightImputation"))
    return(res)
}

check_data_columns <- function(data, ..., stop_on_error = TRUE) {
    columns <- unlist(list(...))
    idx <- (columns %in% colnames(data))
    if (!all(idx)) {
        msg <- paste0("Could not find `", columns[!idx], "` in data.",
                      collapse = "\n")
        if (isTRUE(stop_on_error)) {
            throw(msg)
        } else {
            warning(msg)
            result <- FALSE
        }
    } else {
        result <- TRUE
    }
    return(invisible(result))
}
