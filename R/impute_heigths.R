.impute_heights <- function(data, height_var, dbh_var, species_var,
                            grouping_var, 
                            func = uniform_height) {
    res <- data
    for (spec in unique(data[is.na(data[[height_var]]), species_var])) {
        idx <- data[[species_var]] == spec & is.na(data[[height_var]])
        missing <- data[idx, TRUE]
        measured <- data[!is.na(data[[height_var]]), TRUE]
        if (nrow(measured) == 0) {
            msg <- paste0("Got no height measurement for species ", 
                          missing[[species_var]], " in ", 
                          grouping_var, " ", missing[[grouping_var]], "\n",
                          "You could delete it from our input data set.")
            stop(msg)
        }
        heights <- func(dbh = missing[[dbh_var]], 
                     species = spec,
                     data_dbh = measured[[dbh_var]],
                     data_height = measured[[height_var]]
                     )
        res[idx, "height_imputed_only"] <- heights
    }
    return(res)
}


#' Impute Heights in Data
#'
#' Having a \code{\link{data.frame}} with tree measurements, we want to impute
#' missing heights.
#' @param data A \code{\link{data.frame}} with tree measurements.
#' @param height_var The column in \code{data} giving the measured heights.
#' @param dbh_var The column in \code{data} giving the measured diameters at
#' breast height.
#' @param species_var The column in \code{data} giving the tree species.
#' @param grouping_var The column in \code{data} giving grouping (such as a plot
#' id). Add one if missing!
#' @param func A function for imputing heights. Right now we only have the
#' \code{\link{uniform_height}} function.
#' @return A \code{\link{data.frame}} with imputed heights.
#' @export
#' @examples
#' data(mathisle17)
#' d <- subset(mathisle17, Plot %in% 1:5)
#' try(
#' impute_heights(data = d,
#'                height_var = "Hoehe2017_m",
#'                dbh_var = "BHD2017_cm",
#'                species_var = "Baumart_2017",
#'                grouping_var = "Plot")
#' )
#' try(suggest_uniform_species(d$Baumart_2017))
#' # Ki could mean either `Kirsche` or `Kiefer`... we go for `Kirsche`:
#' d$species <- suggest_uniform_species(species  <-  ifelse(d$Baumart_2017 == "Ki",
#'                                             "Kir",
#'                                             d$Baumart_2017))
#' res <- impute_heights(data = d,
#'                       height_var = "Hoehe2017_m",
#'                       dbh_var = "BHD2017_cm",
#'                       species_var = "species",
#'                       grouping_var = "Plot")
#' res[TRUE, c("Hoehe2017_m", "height_imputed_only", "height_imputed")]
impute_heights <- function(data, height_var, dbh_var, species_var, grouping_var,
                           func = uniform_height) {
    check_data_columns(data = data, height_var, dbh_var,
                       species_var, grouping_var)
    res <- data
    for (g in unique(data[[grouping_var]])) {
        idx <- data[[grouping_var]] == g
        group <- data[idx, TRUE]
        heights <- .impute_heights(data = group, height_var = height_var,
                                   dbh_var = dbh_var, species_var = species_var,
                                   grouping_var = grouping_var,
                                   func = func)
        res[idx, "height_imputed_only"] <- heights[["height_imputed_only"]]
        res[["height_imputed"]] <- ifelse(is.na(res[[height_var]]),
                                             res[["height_imputed_only"]],
                                             res[[height_var]]) 
    }
    return(res)
}

 
