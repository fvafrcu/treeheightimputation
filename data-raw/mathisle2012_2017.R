pkgload::load_all()
mathisle <- read.csv2(system.file("extdata", 
                                  "mathisle_2012-2017_baumdaten_edited_notepad.csv", 
                                  package = "treeHeightImputation"))


# Baeume mit fehlenden BHD aus Inventurdaten entfernen:

index <- which(is.na(mathisle$BHD2017_cm) | mathisle$Baumstatus_2017 == 3 | mathisle$Baumstatus_2017 == 4 | mathisle$BHD2017_cm == 0)
# -> Baeume mit fehlendem BHD oder geerntete Baeume

mathisle <- mathisle[-index,]


# Zwei Dataframes anlegen fuer die zwei Untersuchungszeitpunkte
mathisle12 <- mathisle[,1:11]

head(mathisle12)

mathisle17 <- mathisle[,c(1:5,12:20)]

head(mathisle17)


# Fehlende Baumattribute fuer 2017 von 2012 uebernehmen:

# Baumart:
mathisle12$Baumart <- as.character(mathisle12$Baumart)
mathisle17$Baumart_2017 <- as.character(mathisle17$Baumart_2017)

mathisle17$Baumart_2017[mathisle17$Baumart_2017==""] <- mathisle12$Baumart[mathisle17$Baumart_2017==""]

# Azimut:
mathisle17$Azimut_2017[is.na(mathisle17$Azimut_2017)] <- mathisle12$Azimut[is.na(mathisle17$Azimut_2017)]

# Entfernung:
mathisle17$Entfernung_m_2017[is.na(mathisle17$Entfernung_m_2017)] <- mathisle12$Entfernung_m[is.na(mathisle17$Entfernung_m_2017)]


# Verwendung der Gross/kleinschreibung der Baumarten standardisieren:

lower <- tolower(mathisle17$Baumart_2017)

mathisle17$Baumart_2017[which(lower=="apa")] <- "aPa"
mathisle17$Baumart_2017[which(lower=="bi")] <- "Bi"
mathisle17$Baumart_2017[which(lower=="bu")] <- "Bu"
mathisle17$Baumart_2017[which(lower=="ei")] <- "Ei"
mathisle17$Baumart_2017[which(lower=="es")] <- "Es"
mathisle17$Baumart_2017[which(lower=="fi")] <- "Fi"
mathisle17$Baumart_2017[which(lower=="hbu")] <- "HBu"
mathisle17$Baumart_2017[which(lower=="ki")] <- "Ki"
mathisle17$Baumart_2017[which(lower=="kie")] <- "Ki"
mathisle17$Baumart_2017[which(lower=="ta")] <- "Ta"
mathisle17$Baumart_2017[which(lower=="pa")] <- "Pa"
mathisle17$Baumart_2017[which(lower=="bah")] <- "BAh"
mathisle17$Baumart_2017[which(lower=="vb")] <- "Vb"
mathisle17$Baumart_2017[which(lower=="er")] <- "Er"
mathisle17$Baumart_2017[which(lower=="kir")] <- "Kir"

unique(mathisle17$Baumart_2017)
# -> Keine unterschiedliche Schreibweise mehr, aber was ist GrErl.? Taucht auch nicht in 
# Tabelle in Fachkonzept 1.5 Referenzdaten auf
# (Wahrscheinlich Grau Erle -> auch Weiss Erle (WEr))

a <- which(mathisle17$Baumart_2017=="GrErl.")

mathisle17$Baumart_2017[a] <- "WEr"


# Pa (Pappel) gibt es nicht. Ist entweder aPa (autochtone Pappelart) oder sPa (sonstige Pappel)
a <- which(mathisle17$Baumart_2017=="Pa")

mathisle17$Baumart_2017[a] <- "aPa"
# mathisle17$Baumart_2017[a] <- "sPa"

# sLB (sonstiger Laubbaum) hat keine Koeffizienten (EHK u. KRENN)
b <- which(mathisle17$Baumart_2017=="sLB")

mathisle17 <- mathisle17[-b,]
mathisle12 <- mathisle12[-b,]

usethis::use_data(mathisle12, overwrite = TRUE)
usethis::use_data(mathisle17, overwrite = TRUE)
