#---------------------------------------------------------------------
# Funktionen fuer Hoehenimputation Mathislewaldinventur:


# Hilfsfunktion fuer regula falsi:

# x: zu optimierender Parameter (hg)
# dm: Mittelwert BHD
# hm: Mittelwert gemessene Hoehen
# dg: mittlerer BestandesBHD
# A,B,C: Koeffizienten der EHK

f <- function(x, dm, hm, dg, A, B, C){
  
  (hm - 1.3)*exp(((1/dm) - (1/dg))*(A*dg + B*hm + C)) + 1.3 - x
  
}


# Hilfsfunktion um Koeffizienten der EHK abzurufen:

# Art: Baumart

Koeff_EHK <- function(Art){
  
  aPa <- c(-0.22695,-0.01379,-2.75170)
  Bi <- c(-0.30742,0.07437,-3.38150)
  Bu <- c(-0.23828,0.23430,-9.00290)
  Ei <- c(-0.17147,0.00056,-4.33060)
  Es <- c(-0.21024,0.23745,-8.92620)
  Fi <- c(-0.35218,0.16909,-7.36400)
  HBu <- c(-0.31821,0.38606,-7.42410)
  Ki <- c(-0.26223,-0.00720,-1.51440)
  Ta <- c(-0.24506,0.05366,-7.41290)
  
  koeff <- data.frame(aPa, Bi, Bu, Ei, Es, Fi, HBu, Ki, Ta)
  
  rownames(koeff) <- c("A","B","C")

  if(any(Art %in% c("Fi","Dgl","Lae","SFi","OFi","Pic","Abg","Thu","Tsu","Abi","ELa","JLa","sNB","Eb","uDg"))){Art_temp <- "Fi"}
  if(any(Art %in% c("Ta","NTa"))){Art_temp <- "Ta"}
  if(any(Art %in% c("Ki","SKi","Wey","Pin"))){Art_temp <- "Ki"}
  if(any(Art %in% c("Ei","SEi","TEi","REi","Nu","SNu","EKa","RKa","Rob","Wa","Wb","uHL","Str","Jug"))){Art_temp <- "Ei"}
  if(any(Art %in% c("Bu","HLb"))){Art_temp <- "Bu"}
  if(any(Art %in% c("Bi","aPa","sPa","RPa","WLb","Pop"))){Art_temp <- "Bi"}
  if(any(Art %in% c("HBu","Li","SLi","WLi"))){Art_temp <- "HBu"}
  if(any(Art %in% c("BAh","SAh","FAh","Ul","Pla","Kir","Es","Ah","Fra","TKr","Bul","Ful","Flu"))){Art_temp <- "Es"}
  if(any(Art %in% c("REr","WEr","El","Meb","Vb","Wei","Er","BPa","PrumSpe","Sor"))){Art_temp <- "aPa"}
  
  if(any(!Art %in% c("Fi","Dgl","SFi","OFi","Pic","Abg","Thu","Tsu","Abi","sNB","Eb","uDG","Ta","NTa",
                 "Ki","Lae","SKi","Wey","Pin","ELa","JLa","Ei","SEi","TEi","REi","Nu","SNu","EKa",
                 "RKa","Rob","FAh","Ul","Pla","Kir","aPa","sPa","RPa","El","Meb","Vb","Ah","Wa","Wb",
                 "uHL","Str","TKr","Pru","Jug","Spe","Sor","Bul","Ful","Flu","Bu","HBu","Li","SAh","HLb",
                 "BPa","SLi","WLi","Es","BAh","FAh","Fra","REr","WEr","Er","Pop","Bi","Wei","WLb"))){
    stop("Keine Koeffizienten vorhanden!")
  }else{
    
    id <- which(names(koeff)==Art_temp)
    
    return(koeff[,id])
  }
}


# Hilfsfunktion um Koeffizienten der Hoehentarifs nach KRENN abzurufen:

# Art: Baumart
# Tarif: Tarifhoehe ("oberer","mittlerer","unterer")

Koeff_KRENN <- function(Art, Tarif){
  
  
  Fi <- c(46.18451,51.75440,40.67187,-0.03929,-0.03915,-0.03910,1.36051,1.35469,1.35262)
  Ta <- c(35.30350,39.50755,31.03694,-0.04799,-0.04815,-0.04817,1.34420,1.34842,1.34747)
  Ki <- c(34.56166,38.76946,30.42825,-0.04403,-0.04368,-0.04396,1.17573,1.16748,1.17457)
  Ei <- c(30.17392,33.74597,26.57159,-0.03915,-0.03955,-0.03913,0.83942,0.84676,0.84128)
  Bu <- c(39.34269,44.01971,34.60330,-0.03756,-0.03768,-0.03764,0.96776,0.96926,0.96887)
  Bi <- c(25.93200,29.06536,22.83257,-0.07983,-0.07966,-0.07962,1.31630,1.31449,1.31348)
  Es <- c(39.38461,44.22029,34.81794,-0.02885,-0.02854,-0.02837,0.79364,0.78894,0.78732)
  aPa <- c(52.33822,58.55854,45.86999,-0.00916,-0.00918,-0.00926,0.61835,0.61847,0.61940)
  Er <- c(29.95623,33.53764,26.31457,-0.04447,-0.04447,-0.04487,0.86139,0.86054,0.86782)
  
  TarifKoeff <- as.data.frame(rbind(Fi,Ta,Ki,Ei,Bu,Bi,Es,aPa,Er))
  
  names(TarifKoeff) <- c("A_M","A_O","A_U","B_M","B_O","B_U","C_M","C_O","C_U")
  
  mittlererTarif <- TarifKoeff[,c(1,4,7)]
  obererTarif <- TarifKoeff[,c(2,5,8)]
  untererTarif <- TarifKoeff[,c(3,6,9)]
  
  
  if(any(Art %in% c("Fi","Dgl","SFi","OFi","Pic","Abg","Thu","Tsu","Abi","sNB","Eb","uDG"))){ Art_temp <- "Fi"}
  if(any(Art %in% c("Ta","NTa"))){Art_temp <- "Ta"}
  if(any(Art %in% c("Ki","Lae","SKi","Wey","Pin","ELa","JLa"))){Art_temp <- "Ki"}
  if(any(Art %in% c("Ei","SEi","TEi","REi","Nu","SNu","EKa","RKa","Rob","FAh","Ul","Pla","Kir",
                "aPa","sPa","RPa","El","Meb","Vb","Ah","Wa","Wb","uHL","Str","TKr","Pru","Jug","Spe","Sor","Bul","Ful","Flu"))){Art_temp <- "Ei"}
  if(any(Art %in% c("Bu","HBu","Li","SAh","HLb","BPa","SLi","WLi"))){Art_temp <- "Bu"}
  if(any(Art %in% c("Es","BAh","FAh","Fra"))){Art_temp <- "Es"}
  if(any(Art %in% c("REr","WEr","Er"))){Art_temp <- "Er"}
  if(Art=="Pop"){Art_temp <- "aPa"}
  if(any(Art %in% c("Bi","Wei","WLb"))){Art_temp <- "Bi"}
  
  if(any(!Art %in% c("Fi","Dgl","SFi","OFi","Pic","Abg","Thu","Tsu","Abi","sNB","Eb","uDG","Ta","NTa",
                 "Ki","Lae","SKi","Wey","Pin","ELa","JLa","Ei","SEi","TEi","REi","Nu","SNu","EKa",
                 "RKa","Rob","FAh","Ul","Pla","Kir","aPa","sPa","RPa","El","Meb","Vb","Ah","Wa","Wb",
                 "uHL","Str","TKr","Pru","Jug","Spe","Sor","Bul","Ful","Flu","Bu","HBu","Li","SAh","HLb",
                 "BPa","SLi","WLi","Es","BAh","FAh","Fra","REr","WEr","Er","Pop","Bi","Wei","WLb"))){
    stop(paste("Keine Koeffizienten vorhanden fuer",Art,sep=" "))
  }else{
    
    
    if(Tarif=="mittlerer"){
      
      tempKoeff <- mittlererTarif[which(rownames(mittlererTarif)==Art_temp),]
      
    }
    
    if(Tarif=="oberer"){
      
      tempKoeff <- obererTarif[which(rownames(obererTarif)==Art_temp),]
      
    }
    
    if(Tarif=="unterer"){
      
      tempKoeff <- untererTarif[which(rownames(untererTarif)==Art_temp),]
      
    }  
    
    return(tempKoeff)
  }
}


# Hilfsfunktion um Arten id fuer BDAT abzurufen:

# Art: Baumart

Spp_BDAT <- function(Art){
  
  if(any(Art %in% c("Fi","OFi","Eb"))){Art_temp <- "Fi"}
  if(Art == "SFi"){Art_temp <- "SF"}
  if(any(Art %in% c("Ta","NTa"))){Art_temp <- "Ta"}
  if(Art == "Abg"){Art_temp <- "KT"}
  if(Art == "Ki"){Art_temp <- "Kie"}
  if(Art == "SKi"){Art_temp <- "SK"}
  if(Art == "Wey"){Art_temp <- "WK"}
  if(Art == "sNb"){Art_temp <- "SN"}
  if(any(Art %in% c("Dgl","Pic","Abi","uDg"))){Art_temp <- "DG"}
  
  if(Art == "Lae"){Art_temp <- "La"}
  if(Art == "ELa"){Art_temp <- "EL"}
  if(Art == "JLa"){Art_temp <- "JL"}
  
  if(Art == "Thu"){Art_temp <- "Th"}
  if(Art == "Tsu"){Art_temp <- "Ts"}
  
  if(any(Art %in% c("REi"))){Art_temp <- "RE"}
  if(any(Art %in% c("Ei","SEi","TEi","Nu","SNu","aPa","Meb",
                "HLb","WLb","Wa","Wb","uHL","Str","Pru","Jug","Spe","Sor"))){Art_temp <- "Ei"}
  if(any(Art %in% c("sPa","Pop"))){Art_temp <- "Pa"}
  if(Art == "BPa"){Art_temp <- "BP"}
  if(Art == "HBu"){Art_temp <- "HB"}
  if(Art == "Es"){Art_temp <- "Es"}
  if(Art == "Ah"){Art_temp <- "Ah"}
  if(Art == "BAh"){Art_temp <- "BA"}
  if(Art == "SAh"){Art_temp <- "SA"}
  if(Art == "FAh"){Art_temp <- "FA"}
  if(Art == "Rob"){Art_temp <- "Ro"}
  if(Art == "Bi"){Art_temp <- "Bi"}
  if(Art == "Li"){Art_temp <- "Li"}
  if(any(Art %in% c("Er", "REr", "WEr"))){Art_temp <- "Er"}
  if(Art == "Kir"){Art_temp <- "Kir"}
  if(Art == "Ul"){Art_temp <- "Ul"}
  if(Art == "El"){Art_temp <- "El"}
  if(any(Art %in% c("EKa","RKa"))){Art_temp <- "Ka"}
  if(Art == "Wei"){Art_temp <- "We"}
  if(Art == "sLB"){Art_temp <- "LB"}
  if(Art == "Vb"){Art_temp <- "VB"}
  
  if(any(Art %in% c("Bu","Pla","Fra","TKr","SLi","WLi","Bul","Ful","Flu"))){Art_temp <- "Bu"}
  
  
  if(any(!Art %in% c("Fi","Dgl","SFi","OFi","Pic","Abg","Thu","Tsu","Abi","sNB","Eb","uDG","Ta","NTa",
                 "Ki","Lae","SKi","Wey","Pin","ELa","JLa","Ei","SEi","TEi","REi","Nu","SNu","EKa",
                 "RKa","Rob","FAh","Ul","Pla","Kir","aPa","sPa","RPa","El","Meb","Vb","Ah","Wa","Wb",
                 "uHL","Str","TKr","Pru","Jug","Spe","Sor","Bul","Ful","Flu","Bu","HBu","Li","SAh","HLb",
                 "BPa","SLi","WLi","Es","BAh","FAh","Fra","REr","WEr","Er","Pop","Bi","Wei","WLb"))){
    stop("Keine Koeffizienten vorhanden!")
  }else{
    return(getSpeciesCode(inSp = Art_temp))
  }
}

# Anpassung der EHK an Plot, Ermittlung von BHD bei abweichender Messhoehe:

# BHD: Brusthoehendurchmesser
# H: gemessene Hoehen
# H1: abweichende Messhoehe BHD
# n: Plotnummer (dient nur zur besseren Identifizierung von Plots ohne Referenzhoehe)

EHK <- function(Art, BHD, H, H1, n){
  
  # Koeffizienten fuer EHK generieren:
  
  tempKoeff <- Koeff_EHK(Art = Art)
  
  
  # testen ob ueberhaupt eine Referenzhoehe gemessen wurde mit BHD in korrekter Hoehe
  
  if(any(complete.cases(H))){
    
    
    # BHD korrigieren falls in abweichender Hoehe
    
    if(any(complete.cases(H) & complete.cases(H1))){
      
      id <- which(complete.cases(H) & complete.cases(H1))
    
      spp <- Spp_BDAT(Art=Art)
    
      BHD[id] <- getDiameter(tree=list(spp=spp, D1=BHD[id], H1=H1[id], H=H[id]), Hx=1.3)
    }
    
    # Kallibrieren der EHK:
    
    id <- which(complete.cases(H))
    
    hm <- mean(H[id])
    
    dm <- mean(BHD[id])
    
    dg <- sqrt(sum(BHD[id]^2, na.rm=T)/length(BHD[id]))
    
    hmax <- 1.2*hm
    hmin <- 0.5*hm
    
    regulaFalsi <- uniroot(f=f, interval=c(hmin,hmax), tol=0.0001,# f.lower = -5, f.upper = 5,
                           dm=dm,
                           hm=hm,
                           dg=dg,
                           A=tempKoeff[1],
                           B=tempKoeff[2],
                           C=tempKoeff[3])
    
    hg <- regulaFalsi$root
    
    
    # BHD korrigieren falls es abweichende Messhoehen des BHD gibt ohne gemessene Hoehe
    
    if(any(complete.cases(H1) & is.na(H))){
      
      id2 <- which(complete.cases(H1) & is.na(H)) # wo wurde BHD in einer abweichenden Hoehe gemessen
      
      H1 <- H1[id2]
      
      D1 <- BHD[id2]
      
      BHD_temp_1 <- D1+((H1)-1.3)*1
      
      H_temp_1 <- (hg - 1.3)*exp(((1/BHD_temp_1)-(1/dg))*(tempKoeff[1]*dg + tempKoeff[2]*hg + tempKoeff[3])) + 1.3
      
      spp <- Spp_BDAT(Art=Art)
      
      BHD_temp_2 <- getDiameter(tree = list(spp=spp, D1=D1, H1=H1, H=H_temp_1), Hx=1.3)
      
      # Der naechste Schritt wird von BI vorgeschlagen, ist aber meines erachtens nach sinnlos
      
      # H_final <- (hg - 1.3)*exp(((1/BHD_temp_2)-(1/dg))*(tempKoeff[1]*dg + tempKoeff[2]*hg + tempKoeff[3])) + 1.3
      # BHD_final <- getDiameter(tree = list(spp=spp, D1=BHD_temp_2, H1=rep(1.3,length(id)), H=H_final), Hx=rep(1.3,length(id)))
      
      BHD_final <- BHD_temp_2
      
      BHD[id2] <- BHD_final
    }
    
    # Hoehenberechnen mit kalibrierter EHK
    
    H_temp_2 <- (hg - 1.3)*exp(((1/BHD)-(1/dg))*(tempKoeff[1]*dg + tempKoeff[2]*hg + tempKoeff[3])) + 1.3
    
    H[is.na(H)] <- H_temp_2[is.na(H)]
    
  }else{
    
    message(paste("Keine",Art,"Referenzmessung in Plot",n,sep=" "))
  }
  
  return(list("BHD"=BHD,"H"=H))
}


# Funktion zur Imputation der Hoehe nach EHK und Korrektur abweichender BHD 
# fuer alle Plots und Baumarten

# Art: Baumart
# BHD: Brusthoehendurchmesser
# H: gemessene Hoehen
# Baumart: Vector mit aufgenommenen Baumarten
# Plot: Vector mit erhobenen Plot ids
# H1: abweichende Messhoehe des BHD

HoehenImp <- function(Art, BHD, H, Baumart, Plot, H1){
  
  ArtVec <- unique(Baumart)
  
  temp <- as.data.frame(cbind("Plot"=Plot, "BHD"=BHD, "H1"=H1, "H"=H))
  
  for(Art in ArtVec){
    
    
    artIndex <- which(Baumart==Art)
    
    
    temp_1 <- as.data.frame(temp[artIndex,])
    
    # relevante Plotnummern erzeugen
    
    n <- unique(temp_1$Plot)
    
    # Hoehenimputation pro Plot: 
    
    for(i in 1:length(n)){
      
      plotIndex <- which(temp_1$Plot==n[i])
      
      temp_2 <- as.data.frame(temp_1[plotIndex,])
      
      
      # nach EHK und BHD Korrektur mit BDATPRO
      
      result <-  EHK(Art = Art, BHD = temp_2$BHD, H = temp_2$H, H1 = temp_2$H1, n=n[i]) # Funktion zur Berechnung der Hoehe und Korrektur der BHD
      
      temp_1$BHD[plotIndex] <- result$BHD
      
      temp_1$H[plotIndex] <- result$H
      
    }
    
    temp$BHD[artIndex] <- temp_1$BHD
    
    temp$H[artIndex] <- temp_1$H
    
  }
  
  return(list("BHD"=temp$BHD, "H"=temp$H))
} 


# Funktion zur Berechnung der Tarifhoehe nach KRENN fuer alle BHD

# Baumart: Vector mit erhobenen Baumarten
# Tarif: Tarifhoehe ("oberer","mittlerer","unterer")
# BHD: Brusthoehendurchmesser

KRENN <- function(Baumart, Tarif, BHD, H){
  
  ArtVec <- unique(Baumart)
  
  for(Art in ArtVec){
  tempKoeff <- Koeff_KRENN(Art = Art, Tarif = Tarif)
  
  A <- as.numeric(tempKoeff[1])
  B <- as.numeric(tempKoeff[2])
  C <- as.numeric(tempKoeff[3])
  
  index <- which(Baumart==Art & is.na(H))
  
  H[index] <- (A*(1-exp(B*BHD[index]))^C)
  }
  return(as.numeric(H))
}


# Fehlende Hoehen nach EHK ergaenzen mittels mixed model:

# H: gemessene Hoehen
# H_ei: gemessene Hoehen Erstinventur
# Baumart: Vector mit erhobenen Baumarten
# Plot: Vector mit erhobener Plot ID

MixedModel <- function(H, H_ei, Baumart, Plot){
  
  temp <- data.frame(Baumart=Baumart, Plot=Plot, H=H, H_ei=H_ei)
  
  model <- lmer(H ~ H_ei + (H_ei|Baumart) + (H_ei|Plot), data = temp)
  
  rnd <- ranef(model)
  
  # Plot ermitteln der am naechsten an dem fixed effect liegt -> kleinster random effect
  
  Plot_dummy <- as.numeric(row.names(rnd$Plot)[which.min(abs(rnd$Plot$`(Intercept)`)+abs(rnd$Plot$H_ei))])
  
  temp$Plot[!as.character(temp$Plot) %in% row.names(rnd$Plot)] <- Plot_dummy
  
  # Hoehen vorhersagen auf Basis bestehender Hoehen aus Erstinventur
  
  id <- complete.cases(temp$H_ei)
  
  pred <- predict(model, newdata=temp[id,])
  
  temp$H[id] <- pred
  
  return(temp$H)
}


# Komplettes Hoehenimputationsverfahren mittels EHK und BDAT, danach fehlende Hoehen
# ergaenzen nach zwei Varianten:

# + Variante 1 (var = "model"): mixed effect model auf Basis der Hoehen aus Erstinventur
#    -> anschliessend immer noch bestehende Fehlwerte mit KRENN Hoehentarif
#
# + Variante 2 (var = "tarif"): direkt alle Fehlwerte nach EHK mittels KRENN Hoehentarif ermitteln

# Baumart: Vector mit erhobenen Baumarten
# Plot: Vector mit Plot ID
# H: gemessene Hoehe
# H1: abweichende Messhoehe des BHD
# BHD: Brusthoehendurchmesser
# var: "model";"tarif"
# H_ei: gemessene Hoehen aus Erstinventur
# Tarif: Hoehentarif nach KRENN ("oberer";"unterer";"mittlerer")

HoehenImpComplete <- function(Baumart, Plot, H, H1, BHD, var, H_ei, Tarif){
  
  imputation <- HoehenImp(BHD = BHD,
                          H = H,
                          H1=H1,
                          Baumart = Baumart,
                          Plot=Plot)
  
  if(var=="model"){
    # Mixed Model:
    
    imputation$H <- MixedModel(H = imputation$H,
                               H_ei = H_ei,
                               Baumart = Baumart,
                               Plot = Plot)
    
  }
  # KRENN Hoehentarif:
  
  imputation$H <- KRENN(Baumart = Baumart,
                        Tarif = Tarif,
                        BHD = imputation$BHD,
                        H = imputation$H)
  
  return(imputation)
}

