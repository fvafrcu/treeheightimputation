\name{NEWS}
\title{NEWS}

\section{Changes in version 0.5.0}{
\itemize{
\item Added \code{impute_heights()} to impute heights to incomplete measured (field) data.
}
}

\section{Changes in version 0.4.0}{
\itemize{
\item Having written package regulaFalsi, I choose to use stats::uniroot() only.
It's faster, more reliable and thoroughly tested.
}
}

\section{Changes in version 0.3.0}{
\itemize{
\item Change argument \emph{regula_falsi} to \emph{method}, allowing for stats::uniroot().
}
}

\section{Changes in version 0.2.2}{
\itemize{
\item Set regula_falsi = TRUE by default.
}
}

\section{Changes in version 0.2.1}{
\itemize{
\item Fixed hg(..., regula_falsi = TRUE).
}
}

\section{Changes in version 0.2.0}{
\itemize{
\item Added regula falsi.
}
}

\section{Changes in version 0.1.0}{
\itemize{
\item Added a \code{NEWS.md} file to track changes to the package.
}
}

