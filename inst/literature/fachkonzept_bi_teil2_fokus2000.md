Notizen zu:
FOKUS 2000
Fachkonzept BI, Teil 2
Daten und Algorithmen
Stand: 13.11.2003
Version 1.47
EBZI – Forsten, Kornwestheim
Autor(en): Helge Haug, ab Version 1.23: Arne Nothdurft, FVA,
Ersterstellung: 11.01.2000

# Was ich nicht verstehe
S.19: Definition der Probekreise:
- Wie ist Verj&uuml;ngung definiert?
- 2. Kreis: Ist das identisch zu: alles &uuml;ber 1.3 Meter H&ouml;he? Oder
     sollen die B&auml;ume mit 9.9 cm < BHD <= 10 cm _nicht_ aufgenommen werden?
- Was heisst "ohne Pr&uuml;fung"?


## Einheitsh&ouml;henkurve
2.2.3.4 (Seite 153)
S. 154: Die in der Bemerkung am Seitenanfang referenzierte Variante "Bäume aus Probekreis des Individuums" findet sich weder in Abschnitt 2.2.3.4, per Suche im pdf, sonst irgendwo.
S. 151, Hilfsfunktion:
 - Wie ist tilde(h) definiert? Wohl als hmin,hmax in der Hauptfunktion?
 - Was ist h? Soll das h1 sein?
 - dg soll dg in ehk werden? und bar(d) soll d1.3 in ehk werden? Und das soll
   ein Fachkonzept sein?

# Volumenberechnung
Siehe 1.4.4 (Seite 22)

