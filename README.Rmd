---
output: github_document
---
[![pipeline status](https://gitlab.com/fvafrcu/treeHeightImputation/badges/master/pipeline.svg)](https://gitlab.com/fvafrcu/treeHeightImputation/commits/master)    
[![coverage report](https://gitlab.com/fvafrcu/treeHeightImputation/badges/master/coverage.svg)](https://gitlab.com/fvafrcu/treeHeightImputation/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrcu/treeHeightImputation.svg?branch=master)](https://travis-ci.org/fvafrcu/treeHeightImputation)
    [![Coverage Status](https://codecov.io/github/fvafrcu/treeHeightImputation/coverage.svg?branch=master)](https://codecov.io/github/fvafrcu/treeHeightImputation?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/treeHeightImputation)](https://cran.r-project.org/package=treeHeightImputation)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/treeHeightImputation)](https://cran.r-project.org/package=treeHeightImputation)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/treeHeightImputation)](https://cran.r-project.org/package=treeHeightImputation)

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
```

# treeHeightImputation
## Introduction
Please read the
[vignette](https://fvafrcu.gitlab.io/treeHeightImputation/inst/doc/An_Introduction_to_treeHeightImputation.html).

Or, after installation, the help page:
```{r, eval = FALSE}
help("treeHeightImputation-package", package = "treeHeightImputation")
```
```{r, echo = FALSE}
  # insert developement page
  help_file <- file.path("man", "treeHeightImputation-package.Rd")
  captured <- gsub('_\b', '',  capture.output(tools:::Rd2txt(help_file) ))
  cat(captured, sep = "\n")
```

## Installation

You can install treeHeightImputation from github with:

```{r gh-installation, eval = FALSE}
if (! require("devtools")) install.packages("devtools")
devtools::install_git("https://gitlab.com/fvafrcu/treeHeightImputation")
```


